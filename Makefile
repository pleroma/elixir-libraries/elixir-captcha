CFLAGS ?= -g

HEADERS = src/colors.h src/font.h

priv/captcha: priv src/captcha.c $(HEADERS)
	mkdir -p priv
	$(CC) -std=c99 $(CFLAGS) -o priv/captcha src/captcha.c $(LDFLAGS) $(LDLIBS)

.PHONY: clean
clean:
	rm -f priv/captcha $(BEAM_FILES)
