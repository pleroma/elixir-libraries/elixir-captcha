defmodule Mix.Tasks.Compile.Make do
  def run(_) do
    {result, error_code} = System.cmd("make", [], stderr_to_stdout: true)
    Mix.shell().info(result)

    case error_code do
      0 -> :ok
      _ -> {:error, ["make failed"]}
    end
  end
end

defmodule Mix.Tasks.Clean.Make do
  def run(_) do
    {result, error_code} = System.cmd("make", ["clean"], stderr_to_stdout: true)
    Mix.shell().info(result)

    case error_code do
      0 -> :ok
      _ -> {:error, ["make clean failed"]}
    end
  end
end

defmodule Captcha.Mixfile do
  use Mix.Project

  def project do
    [
      app: :captcha,
      version: "1.0.0",
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      compilers: [:make, :elixir, :app],
      description: description(),
      aliases: aliases(),
      package: package(),
      test_coverage: [summary: [threshold: 50]],
      deps: deps()
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/p-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    []
  end

  defp aliases do
    [clean: ["clean", "clean.make"]]
  end

  defp description do
    """
    This is a Elixir lib for generating captcha. No dependencies. It drawing captcha image with C code. No ImageMagick, No RMagick.

    This is the fork used by Pleroma.
    """
  end

  defp package do
    [
      name: :pleroma_captcha,
      files: [
        "lib",
        "priv",
        "mix.exs",
        "README*",
        "LICENSE*",
        "src",
        "test",
        "config",
        "Makefile"
      ],
      maintainers: ["pleroma"],
      licenses: ["MIT"],
      links: %{
        "GitHub" => "https://git.pleroma.social/pleroma/elixir-libraries/elixir-captcha",
        "Docs" => "https://git.pleroma.social/pleroma/elixir-libraries/elixir-captcha"
      }
    ]
  end
end
